﻿using System;

namespace JokeGenerator.Extensions
{
    public static class StringHelper
    {
        public static void ConsolePrint(this object printValue)
        {
            Console.WriteLine("\r" + printValue);
        }
    }
}