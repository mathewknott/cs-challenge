﻿using System;
using JokeGenerator.Extensions;
using JokeGenerator.Services;

namespace JokeGenerator
{
    class Program
    {
        private static char _key;

        private static Tuple<string, string> _names;

        static void Main()
        {
            var jokesFeed = new JokesFeed("https://api.chucknorris.io/");
            "Press ? to get instructions.".ConsolePrint();

            if (Console.ReadLine() == "?")
            {
                while (true)
                {
                    "Press c to get categories".ConsolePrint();

                    "Press r to get random jokes".ConsolePrint();

                    GetEnteredKey(Console.ReadKey());

                    if (_key == 'c')
                    {
                        string.Join(", ", string.Join(", ", jokesFeed.GetCategories().Result)).ConsolePrint();
                    }
                    if (_key == 'r')
                    {
                        "Want to use a random name? y/n".ConsolePrint();

                        GetEnteredKey(Console.ReadKey());

                        if (_key == 'y')
                        {
                            var namesFeed = new NamesFeed("http://uinames.com/api/");
                            var result = namesFeed.GetName().Result;
                            _names = Tuple.Create(result.Name, result.Surname);
                        }

                        "Want to specify a category? y/n".ConsolePrint();

                        if (_key == 'y')
                        {
                            "How many jokes do you want? (1-9)".ConsolePrint();

                            var n = Console.ReadLine();

                            string.Join(", ", string.Join(", ", jokesFeed.GetCategories().Result)).ConsolePrint();

                            "Enter a category;".ConsolePrint();

                            var result = int.TryParse(n, out var number);

                            if (result)
                            {
                                var categoryInput = Console.ReadLine();

                                for (var i = 0; i < number; i++)
                                {
                                    jokesFeed.GetRandomJoke(_names?.Item1, _names?.Item2, categoryInput).Result
                                        .ConsolePrint();
                                }
                            }
                        }
                        else
                        {
                            "How many jokes do you want? (1-9)".ConsolePrint();

                            var n = Console.ReadLine();

                            var result = int.TryParse(n, out var number);

                            if (result)
                            {
                                for (var i = 0; i < number; i++)
                                {
                                    string.Join(", ", jokesFeed.GetRandomJoke(_names?.Item1, _names?.Item2).Result).ConsolePrint();
                                }
                            }
                        }
                    }
                    _names = null;
                }
            }
        }

        private static void GetEnteredKey(ConsoleKeyInfo consoleKeyInfo)
        {
            switch (consoleKeyInfo.Key)
            {
                case ConsoleKey.C:
                    _key = 'c';
                    break;
                case ConsoleKey.D0:
                    _key = '0';
                    break;
                case ConsoleKey.D1:
                    _key = '1';
                    break;
                case ConsoleKey.D3:
                    _key = '3';
                    break;
                case ConsoleKey.D4:
                    _key = '4';
                    break;
                case ConsoleKey.D5:
                    _key = '5';
                    break;
                case ConsoleKey.D6:
                    _key = '6';
                    break;
                case ConsoleKey.D7:
                    _key = '7';
                    break;
                case ConsoleKey.D8:
                    _key = '8';
                    break;
                case ConsoleKey.D9:
                    _key = '9';
                    break;
                case ConsoleKey.R:
                    _key = 'r';
                    break;
                case ConsoleKey.Y:
                    _key = 'y';
                    break;
            }
        }

    }
}