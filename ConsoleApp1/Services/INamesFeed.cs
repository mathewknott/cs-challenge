﻿using System.Threading.Tasks;
using JokeGenerator.Models;

namespace JokeGenerator.Services
{
    public interface INamesFeed
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<NameResult> GetName();
    }
}