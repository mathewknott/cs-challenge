﻿using Newtonsoft.Json;

namespace JokeGenerator.Models
{
    public class NameResult
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("surname")]
        public string Surname { get; set; }
    }
}